package com.jamieswhiteshirt.literalascension.core.patcher;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.launchwrapper.IClassTransformer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.objectweb.asm.*;

public class LiteralAscensionASM implements IClassTransformer {

    @Override
    public byte[] transform(String name, String transformedName, byte[] basicClass) {
        if (name.equals("net.minecraftforge.common.ForgeHooks")) {
            ClassReader classReader = new ClassReader(basicClass);
            ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_MAXS);
            ClassVisitor cv = new Visitor(classWriter);
            classReader.accept(cv, ClassReader.EXPAND_FRAMES);
            return classWriter.toByteArray();
        }
        return basicClass;
    }

    public class Visitor extends ClassVisitor {
        public Visitor(ClassWriter writer) {
            super(Opcodes.ASM4, writer);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
            MethodVisitor visitor = super.visitMethod(access, name, desc, signature, exceptions);
            if (name.equals("isLivingOnLadder")) {
                return new MethodVisitor(Opcodes.ASM4, visitor) {
                    @Override
                    public void visitCode() {
                        mv.visitIntInsn(Opcodes.ALOAD, 0);
                        mv.visitIntInsn(Opcodes.ALOAD, 1);
                        mv.visitIntInsn(Opcodes.ALOAD, 2);
                        mv.visitIntInsn(Opcodes.ALOAD, 3);
                        mv.visitMethodInsn(Opcodes.INVOKESTATIC, toInternalClassName(LiteralAscensionHooks.class.getName()), "isLivingOnLadder", toMethodDescriptor("Z", IBlockState.class.getName(), World.class.getName(), BlockPos.class.getName(), EntityLivingBase.class.getName()), false);
                        mv.visitInsn(Opcodes.IRETURN);
                    }
                };
            }
            return visitor;
        }
    }

    private static String toInternalClassName(String className) {
        return className.replace('.', '/');
    }

    private static boolean isDescriptor(String descriptor) {
        return descriptor.length() == 1 || (descriptor.startsWith("L") && descriptor.endsWith(";"));
    }

    private static String toDescriptor(String className) {
        return isDescriptor(className) ? className : "L" + toInternalClassName(className) + ";";
    }

    private static String toMethodDescriptor(String returnType, String... paramTypes) {
        StringBuilder paramDescriptors = new StringBuilder();
        for (String paramType : paramTypes)
            paramDescriptors.append(toDescriptor(paramType));
        return "(" + paramDescriptors.toString() + ")" + toDescriptor(returnType);
    }
}